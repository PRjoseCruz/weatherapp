var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    liveReload = require('gulp-livereload');

function errorLog(error){

    console.error.bind(error);
    this.emit('end');

}

gulp.task('scripts', function(){
    
    gulp.src('scripts/*.js')
        .pipe(uglify())
        .on('error', errorLog)
        .pipe(gulp.dest('build/js'));

})

gulp.task('styles', function(){

});

//WATCH TASK
//Watches JS
gulp.task('watch', function(){

    var server = liveReload();
    

    gulp.watch('scripts/*.js', ['scripts']);

}) ;


gulp.task('default', ['scripts', 'styles', 'watch']); 

 