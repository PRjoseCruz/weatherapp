//CONTROLLERS
//Home Controller -> main page | passes on city name to get weather
//data
//Scope -> 	city (City name) |
//			watch[checks entry box city to pass on to next cont.]
weatherApp.controller('homeController', ['$scope', '$location',
'cityService', function($scope, $location, cityService){

	$scope.city = cityService.city;
	$scope.$watch('city', function(){

		cityService.city = $scope.city;

	});

	$scope.submit = function(){
	
		$location.path("/forecast");

	};

}]);

//Forecast Controller -> forecast page | gets city name and uses it
//to get data from API |
//Scope -> 	city (City name) | 
//			number (number of instances of a forecast) | 
//			weatherAPI (connection to info) |
//			weatherResults (JSON data of weatherAPI) |
//			convertToCelcius [temp in Kelvin -> temp in celcius]
//			getDate [API JSON file unformated date -> readable date]
weatherApp.controller('forecastController',  ['$scope','$routeParams', 
	'cityService', 'weatherService', function($scope, $routeParams, 
		cityService, weatherService){

	$scope.city = cityService.city;

	$scope.number = $routeParams.number || '2';

	$scope.weatherResults = weatherService.getWeather($scope.city, 
		$scope.number);

	$scope.convertToCelcius = function(tempK){

		tempC = Math.round((tempK - 273.15)*100)/100;
		tempC = parseFloat(tempC).toFixed(2);

		return tempC;

	}

	$scope.getDate = function(dt){

		return new Date(dt * 1000);

	}

	
}]);