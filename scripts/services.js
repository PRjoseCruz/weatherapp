//SERVICES
weatherApp.service('cityService', function(){

	this.city = "Guatemala City";

});

weatherApp.service('weatherService', ['$resource', function($resource){

	this.getWeather = function(city, number){

		var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast?APPID=ab9b9f3c69425fd7aa04c51bc9b172df", 
			{callback: "JSON_CALLBACK"}, 
			{get: {method: "JSONP"}}
		);

		return weatherAPI.get({ q: city,  cnt: number });
	};

}]);